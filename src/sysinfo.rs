// Copyright 2015-2020 Capital One Services, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! # Key-Value Store
//!
//! This module contains the key-value store through which guest modules access
//! the currently bound `wascap:keyvalue` capability provider

use crate::HandlerResult;
use codec::sysinfo::*;
use codec::{deserialize, serialize};
use wapc_guest::host_call;
use std::collections::HashMap;
use wascc_codec as codec;

const CAPID_KEYVALUE: &str = "wascc:sysinfo";

/// An abstraction around a host runtime capability for a key-value store
pub struct SysInfoHostBinding {
    binding: String,
}

impl Default for SysInfoHostBinding {
    fn default() -> Self {
        SysInfoHostBinding {
            binding: "default".to_string(),
        }
    }
}

/// Creates a named host binding for the key-value store capability
pub fn host(binding: &str) -> SysInfoHostBinding {
    SysInfoHostBinding {
        binding: binding.to_string(),
    }
}

/// Creates the default host binding for the key-value store capability
pub fn default() -> SysInfoHostBinding {
    SysInfoHostBinding::default()
}

impl SysInfoHostBinding {
    /// Obtains a single value from the store
    pub fn get_all_sys_info(&self) -> HandlerResult<Option<HashMap<String, String>>> {
        let mut sysinfo_results = HashMap::new();

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_BOOT_TIME,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("BOOT_TIME"), resp.body)
        }).expect("Could not get boot time");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_NCPU,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("NUM_CPU"), resp.body)
        }).expect("Could not get num cpu");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_CPU_SPEED,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("CPU_SPEED"), resp.body)
        }).expect("Could not get cpu speed");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_DISK_INFO,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("DISK_INFO"), resp.body)
        }).expect("Could not get disk info");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_LOAD_AVG,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                    sysinfo_results.insert(String::from("LOAD_AVG"), resp.body)
            }).expect("Could not get load avg");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_MEM_INFO,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("MEM_INFO"), resp.body)
            }).expect("Could not get mem info");

        host_call(&self.binding, CAPID_KEYVALUE, OP_GET_PROC_TOTAL,
            &serialize(SysInfoRequest{body: "".to_string()})?)
            .map(|vec| {
                let resp = deserialize::<SysInfoRequest>(vec.as_ref()).unwrap();
                sysinfo_results.insert(String::from("PROC_TOTAL"), resp.body)
            }).expect("Could not get proc total");

        Ok(Some(sysinfo_results))
    }
}